def function1(a):
    """ function 1

    Params
    ------
    a : int
        blabla
    
    .. jupyter-execute::
   
        import numpy as np
        
        import matplotlib.pyplot as plt

        x = np.linspace(1E-3, 2 * np.pi)

        plt.plot(x, np.sin(x) / x)
        plt.plot(x, np.cos(x))
        plt.grid()
    """
