Pybarracuda project
===================

pybarracuda is a python package for the BARRACUDA software

## Features

- Pretreat files for BARRACUDA
- Run the BARRACUDA software
- Postreat result binary files from BARRACUDA

## Prerequirements

At the moment, BARRACUDA only works on ubuntu

## Instaling

To install the package simply use the pip command:

    pip3 install git+https://gitlab.com/kostarisk/barracuda/pybarracuda


## Installing for beginner

Install anaconda from https://anaconda.org/

Open your anaconda prompt

Create a new conda env

    conda create --name MyEnvironmentName

Activate your new env

    conda activate MyEnvironmentName

Install git

    conda install git

Install pip

    conda install pip

Install pybarracuda

    pip3 install git+https://gitlab.com/kostarisk/barracuda/pybarracuda

## Warning

if error sh: ./main Permission denied in Ubuntu you should give right permissions by typing in the terminal :
chmod +x /usr/local/lib/python3.7/dist-packages/pybarracuda/CodeSource/main

## Main developpers

- Danglade Nikola : nikola.danglade@suez.com
- Fatima-Zahra Mihami : fatima-zahra.mihami@etud.univ-pau.fr