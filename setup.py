from setuptools import setup, find_packages

setup(name="pybarracuda",
	version="0.0.10",
    packages=find_packages(exclude=['tests*']),
    include_package_data=True,
	description='A python package for the BARRACUDA software',
    long_description=open('README.md').read(),
    install_requires=["netCDF4", "numpy", "pandas", "pathlib", "xarray"],
    url='https://gitlab.com/kostarisk/barracuda/pybarracuda',
    author='Nikola Danglade, Fatima-Zahra Mihami',
    author_email='nikola.danglade@suez.com, fatima-zahra.mihami@etud.univ-pau.fr'
)
