
def function1():
    """ description

    The xarray.Dataset to extend must contain the followings coordinates and data 
    variables.

    Coordinates
    -----------
    time : str
        Time of the simulation

    Data variables
    --------------
    h : xarray.DataArray
        Surface elevation h.
    
    Example
    -------
    
    .. jupyter-execute::
   
        import numpy as np
        
        import matplotlib.pyplot as plt

        x = np.linspace(1E-3, 2 * np.pi)

        plt.plot(x, np.sin(x) / x)
        plt.plot(x, np.cos(x))
        plt.grid()   
    """

    print("niko")