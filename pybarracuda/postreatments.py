""" Postreat binary result files from BARRACUDA """

import os
import numpy as np
import pandas as pd
import xarray as xr

from pathlib import Path
from netCDF4 import Dataset

def gauges_to_dataframe(p_results_folder):
    """Transform the gauge results to a pandas dataframe

    Parameters
    ----------
    p_results_folder : str
        The path to the output result folder of the model 

    Returns
    -------
    df : pandas.DataFrame
        The associated pandas dataframe

    Example
    -------

        >>> from pybarracuda.postreatments import gauges_to_dataframe
        >>> df_gauges = gauges_to_dataframe("Results")
    """
    
    # read info.bin file
    p_target = p_results_folder + "/info.bin"
    info = np.fromfile(p_target, dtype=np.float32)
    
    # create time variables
    dt_gauges = info[7]
    time_end = info[1]
    number_time_point = int(time_end/dt_gauges) + 1
    time_gauges = np.linspace(0, time_end, number_time_point)
    
    # load file gauge data
    nbr_file_gauges = 0
    dir_path = p_results_folder + "/TimeSeries"
    for root, dirs, files in os.walk(dir_path):
        for file in files:
            if file.startswith('Hts_'):
                nbr_file_gauges+=1
    
    eta_gauges = []
    for ng in np.arange(0, nbr_file_gauges):
        namefile = dir_path + "/Hts_" + str(ng) + ".bin"
        if Path(namefile).exists():
            eta_gauges.append(np.fromfile(namefile,dtype=np.float32))
       
    u_gauges = []
    for ng in np.arange(0, nbr_file_gauges):
        namefile = dir_path + "/Uts_" + str(ng) + ".bin"
        if Path(namefile).exists():
            u_gauges.append(np.fromfile(namefile,dtype=np.float32))
       
    v_gauges = []
    for ng in np.arange(0, nbr_file_gauges):
        namefile = dir_path + "/Vts_" + str(ng) + ".bin"
        if Path(namefile).exists():
            v_gauges.append(np.fromfile(namefile,dtype=np.float32))
      
    # create lists
    name, x, y, z, time, h, u, v = [], [], [], [], [], [], [], []
    for i_g in np.arange(0, nbr_file_gauges):
        name.append(u"gauge{:.0f}".format(i_g+1))
        
        x.append(eta_gauges[i_g][0])
        y.append(eta_gauges[i_g][1])
        z.append(eta_gauges[i_g][2])
        
        time.append(time_gauges)
        
        if len(eta_gauges) != 0:
            h.append(eta_gauges[i_g][3:])
        else:
            h.append(np.zeros(number_time_point))
            
        if len(u_gauges) != 0:
            u.append(u_gauges[i_g][3:])
        else:
            u.append(np.zeros(number_time_point))
            
        if len(v_gauges) != 0:
            v.append(v_gauges[i_g][3:])
        else:
            v.append(np.zeros(number_time_point))
            
    # create dictt
    dictt = {"gauge": name,
             "x": x,
             "y": y,
             "z" : z,
             "time": time,
             "h": h,
             "u": u,
             "v": v,
             }

    # create dataframe
    df = pd.DataFrame(dictt)

    return df

def gauges_to_xarray(p_results_folder):
    """Transform the gauge results into one xarray Dataset

    Parameters
    ----------
    p_results_folder : str
        The path to the output result folder of the model 

    Returns
    -------
    ds : xarray.Dataset
        The xarray.Dataset of the BARRACUDA gauges

    Examples
    --------
    Example 1 : read gauges

        from pybarracuda.postreatments import gauges_to_xarray
        df_gauges = gauges_to_xarray("Results")
    """

    # load gauges into dataframe
    df = gauges_to_dataframe(p_results_folder)

    # treat data
    time = df.time[0]
    name = df.gauge.to_numpy()
    xm = df.x.to_numpy()
    ym = df.y.to_numpy()
    h = np.array([df.h[i] for i in range(len(name))])
    
    # create xarray dataset
    ds = xr.Dataset(
        data_vars=dict(
            h=(["gauge", "time"], h),
        ),
        coords=dict(
            gauge=(["gauge"], name),
            time=(["time"], time),
            xm=(["gauge"], xm),
            ym=(["gauge"], ym),
        ),
        attrs=dict(description="Barracuda gauges data"),
    )
        
    # add u
    if len(df.u[0])!=0:
        u = np.array([df.u[i] for i in range(len(name))])
        ds = ds.assign(u = (["gauge", "time"], u))
    
    # add v
    if len(df.v[0])!=0:
        v = np.array([df.v[i] for i in range(len(name))])
        ds = ds.assign(v = (["gauge", "time"], v))

    return ds

def result_to_numpy(p_file, p_info):
    """Transform ONE binary result file to ONE numpy array

    The results are based on a gridded grid of nx*ny points, so that : 
    the 2D arrays dimensions are (ny, nx)

    Parameters
    ----------
    p_file : str
        The path to the binary file to transform to a numpy array
    p_info : str
        The path to the Barracuda info.bin file

    Returns
    -------
    B : numpy.array
        The associated np.array. The dimensions are (ny, nx).

    Examples
    --------
    Example 1 : load result H_0.bin

        from pybarracuda.postreatments import result_to_numpy
        B = result_to_numpy("Results/FreeSurface/H_0.bin", "Results/info.bin")
        
    """

    # Read info file
    info = np.fromfile(p_info, dtype=np.float32)
    
    # Read result binary file
    data_array = np.fromfile(p_file, dtype=np.float32)
    
    # Define number of grid ny and nx
    nx=int(info[4])
    ny=int(info[5])
    
    # Define numpy array
    B = np.zeros((nx, ny))
    
    with open(p_file, 'rb') as fid:
        data_array = np.fromfile(fid, np.float32)
    
    # Reshape
    B = np.reshape(data_array, (nx, ny))
    B = B.T

    return B

def results_to_numpy(p_res):
    """ Convert all binary result files to numpy arrays

    The results are based on a gridded grid of ny*nx points and a time
    vector of nt points, so that : the 2D arrays dimensions are (ny, nx), 
    the 3D arrays dimensions are (ny, nx, nt)

    Parameters
    ----------
    p_res : str
        The path to the BARRACUDA folder containing the results
  
    Returns
    -------
    a : numpy.array
        a

    """
    
    p_res = 1
    
    a=1

    return a