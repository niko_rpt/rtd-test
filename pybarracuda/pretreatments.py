"""Pretreat files for BARRACUDA"""

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.subplots
import pyproj
import scipy.integrate
import xarray as xr


def efth_to_barracuda(ds_efth, run, run_duration, grid_x0y0_lon, grid_x0y0_lat,
    grid_xendy0_lon, grid_xendy0_lat, h_wavemaker, ddir, p_out):
    """Convert a xarray dataset efth to a text file for BARRACUDA
    
    Parameters
    ----------
    ds_efth : xarray.Dataset
        Frequency direction spectrum efth contain in a xarray.Dataset.
    ds_efth_freq : xarray.DataArray
        Frequencies of the ds_efth in Hz.
    ds_efth_dir : xarray.DataArray
        Directions of the ds_efth in deg.
    ds_efth_efth : xarray.DataArray
        Energy values of the ds_efth in m^2*s/deg        
    run : str, {"1D", "2D"}
        Type of the run, 1D = grid along one direction x or 2D = grid along two 
        directions x and y.
    run_duration : int or float
        Duration of the run in s.
    grid_x0y0_lon : float
        Longitude grid position of cell in x=0, y=0 in WGS84.
    grid_x0y0_lat : float 
        Latitude grid position of cell in x=0, y=0 in WGS84.
    grid_xendy0_lon : float
        Longitude grid position of cell in x=end, y=0 in WGS84.
    grid_xendy0_lat : float 
        Latitude grid position of cell in x=end, y=0 in WGS84.
    h_wavemaker : int or float
        Depth at the wavemaker in m.
    ddir : int or float
        Directionnal bearing to interpolate the directions on in deg.
    p_out : str
        Path to save the text file for BARRACUDA.
    
    Returns
    -------
    fig1 : plotly.graph_objects.Figure
        Plotly figure of ef after all treatments.
    fig2 : plotly.graph_objects.Figure
        Plotly figure of efths along the several treatments perform before the 
        projection inside the local grid referential.
    fig3 : plotly.graph_objects.Figure
        Plotly figure of efths along the several treatments perform inside the local 
        grid referential.

    Examples
    --------
    Example 1 : create efth file for BARRACUDA

        import pybarracuda.pretreatments
        import wavespectra

        # read efth from swan
        dset_efth = wavespectra.read_swan("swan_spec.spec")

        # type of the run
        run="2D"

        # TotalComputationTime in seconds
        run_duration = 5400

        # grid corners
        grid_x0y0_lon, grid_x0y0_lat = -1.613224, 43.495801444701760
        grid_xendy0_lon, grid_xendy0_lat = -1.56819, 43.472000

        # h at the wavemaker in metre
        h_wavemaker=19

        # ddir to interpolate the directions in degrees
        ddir = 5

        # text file out for BARRACUDA
        p_out = "spectrum.txt"

        # compute spectrum for BARRACUDA
        fig1, fig2, fig3 = pybarracuda.pretreatments.efth_to_barracuda(
            ds_efth, run, run_duration, grid_x0y0_lon, grid_x0y0_lat, grid_xendy0_lon,
            grid_xendy0_lat, h_wavemaker, ddir, p_out,
            )
    """

   a=1

def matrix_to_binary_file(x, y, matrix, path_bin):    
    """Convert and save a numpy matrix to a binary file for BARRACUDA
    
    Parameters
    ----------
    x : numpy.array
        One-dimensional array contaning the domain coordinates in x-direction
        x-direction is by default the main direction of the wave
        Array dimension is nx        
    y : numpy.array
        One-dimensional array contaning the domain coordinates in y-direction
        Array dimension is ny        
    matrix : numpy.array
        Two-dimensional array contaning a function of x and y
        Array dimension is (ny, nx)        
    path_bin : str
        Path and name of the binary file where the matrix data will be saved
        
        
    Examples
    --------
    Example 1 : create binary file

        from pybarracuda.pretreatments import matrix_to_binary_file

        import numpy as np

        # define x and y axes
        N, M = 10, 10
        x = np.linspace(0, 2, N)    # The main direction of the wave
        y = np.linspace(0, 2, M)

        # define the matrix
        X, Y = np.meshgrid(x, y)
        matrix = (X-1)**2 + (Y-1)**2

        # define the binary file
        path_bin = "example.bin"

        # save the matrix as a binary file to be used later in the model
        matrix_to_binary_file(x, y, matrix, path_bin)    
    """
    
    # define x and y dimensions
    nx = np.size(x)
    ny = np.size(y)
    
    # convert matrix into array
    matrix = np.transpose(matrix)
    array = matrix.reshape(nx*ny)
    
    # save array as binary file with single precision
    array.astype('float32').tofile(path_bin)

def timeseries_to_text_file(time, data, path_text):
    """Convert a timeseries to a text file for BARRACUDA
    
    Parameters
    ----------
    time : numpy.array
        One-dimensional array contaning the time of the timeseries        
    data : numpy.array
        One-dimensional array contaning the data of the timeseries
    path_bin : str
        Path and name of the text file where the timeseries will be saved
        
        
    Examples
    --------
    Example 1 : create text file
    
        from pybarracuda.pretreatments import timeseries_to_text_file

        import numpy as np

        # create time
        time = np.arange(0, 100, 1)

        # create data
        data = np.sin(time)

        # save the timeseries to a text file for BARRACUDA
        timeseries_to_text_file(time, data, "example.txt")
    """

    # create dict
    dictt = {"time": time,
        "data": data}
    
    # create dataframe
    df = pd.DataFrame(dictt)

    # save
    df.to_csv(path_text, sep="\t", header=None, index=None)
