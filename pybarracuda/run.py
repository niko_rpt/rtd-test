""" Run the BARRACUDA software """

import os
import pathlib

def run_cpu(p_steering):
    """Run the BARRACUDA CPU software using python

    Parameters
    ----------
    p_steering : str
        The path to the steering.txt file
    
    Examples
    --------
    Example 1 : run

        from pybarracuda.run import run_cpu
        run_cpu("Inputs\Steering.txt")
    """

    # run
    p_main = str(pathlib.Path(__file__).parent.resolve()) + "/source"
    os.chdir(p_main)
    os.system("./main_cpu " + p_steering)

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.subplots
import pyproj
import scipy.integrate
import xarray as xr

def efth_to_txt(ds_efth, run, run_duration, grid_x0y0_lon, grid_x0y0_lat,
    grid_xendy0_lon, grid_xendy0_lat, h_wavemaker, ddir, p_txt):
    """ Convert a xarray dataset efth to a text file for Barracuda
    
    Parameters
    ----------
    ds_efth : xarray.Dataset
        Frequency direction spectrum efth contain in a xarray.Dataset.
    ds_efth_freq : xarray.DataArray
        Frequencies of the ds_efth in Hz.
    ds_efth_dir : xarray.DataArray
        Directions of the ds_efth in deg.
    ds_efth_efth : xarray.DataArray
        Energy values of the ds_efth in m^2*s/deg        
    run : str, {"1D", "2D"}
        Type of the run, 1D = grid along one direction x or 2D = grid along two 
        directions x and y.
    run_duration : int or float
        Duration of the run in s.
    grid_x0y0_lon : float
        Longitude grid position of cell in x=0, y=0 in WGS84.
    grid_x0y0_lat : float 
        Latitude grid position of cell in x=0, y=0 in WGS84.
    grid_xendy0_lon : float
        Longitude grid position of cell in x=end, y=0 in WGS84.
    grid_xendy0_lat : float 
        Latitude grid position of cell in x=end, y=0 in WGS84.
    h_wavemaker : int or float
        Depth at the wavemaker in m.
    ddir : int or float
        Directionnal bearing to interpolate the directions on in deg.
    p_txt : str
        Path to save the text file for Barracuda.
    
    Returns
    -------
    fig1 : plotly.graph_objects.Figure
        Plotly figure of ef after all treatments.
    fig2 : plotly.graph_objects.Figure
        Plotly figure of efths along the several treatments perform before the 
        projection inside the local grid referential.
    fig3 : plotly.graph_objects.Figure
        Plotly figure of efths along the several treatments perform inside the local 
        grid referential.

    Example
    -------

    >>> import pybarracuda.pre
    >>> import wavespectra
    >>> # read efth from swan
    >>> dset_efth = wavespectra.read_swan("swan_spec.spec")
    >>> # type of the run
    >>> run="2D"
    """

    # step 1 : create xarray
    
    # create efth xarray.DataArray
    efth = xr.DataArray(
        data=ds_efth.efth.data,
        dims=["freq","dir"],
        coords=dict(
            freq=(["freq"], ds_efth.freq.data, {"units":"Hz"}),
            dir=(["dir"], ds_efth.dir.data, {"units":"deg"}),
        ),
        attrs=dict(
            units="m2/Hz/deg",
        ),
    )

    # save
    efth_swan_in_degrees = efth

    # compute hs
    ef = scipy.integrate.trapz(efth.data, efth.dir)
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_in_degrees = 4*np.sqrt(m0)

    # step 2 : interpolate efth based on new freq and new direction
    # new df
    df = 1/run_duration

    # new freq
    freq = np.arange(efth.freq.min(dim="freq"), efth.freq.max(dim="freq"), df)

    # new dir
    dir = np.arange(efth.dir.min(dim="dir"), efth.dir.max(dim="dir"), ddir)

    # interpolate efth
    efth = efth.interp(freq=freq, dir=dir, method="linear")

    # save
    efth_swan_interp_in_degrees = efth

    # compute hs
    ef = scipy.integrate.trapz(efth.data, efth.dir)
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_interp_in_degrees = 4*np.sqrt(m0)

    # # step 3 : convert direction into the grid referential

    # compute forward azimuth
    geodesic = pyproj.Geod(ellps='WGS84')
    fwd_azimuth, a, aa = geodesic.inv(
        grid_xendy0_lon, grid_xendy0_lat, grid_x0y0_lon, grid_x0y0_lat)

    # project directions into the grid's referential
    dir = dir - fwd_azimuth

    # apply conventions for the Barracuda wavemaker
    dir = dir%360

    # create efth xarray.DataArray
    efth = xr.DataArray(
        data=efth,
        dims=["freq","dir"],
        coords=dict(
            freq=(["freq"], freq, {"units":"Hz"}),
            dir=(["dir"], dir, {"units":"deg"}),
        ),
        attrs=dict(
            units="m2/Hz/deg",
        ),
    )
    efth = efth.sortby("dir")

    # save
    efth_swan_interp_in_grid_in_degrees = efth

    # compute hs
    ef = scipy.integrate.trapz(efth.data, efth.dir)
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_interp_in_grid_in_degrees = 4*np.sqrt(m0)

    # setp 3 : convert direction from degrees to radian

    # convert energy
    efth = efth*np.pi/180
    efth.attrs["units"] ="m2/Hz/rad"

    # convert direction
    efth = efth.assign_coords(dir=(["dir"], efth.dir.data*np.pi/180, {"units":"rad"}))

    # save
    efth_swan_interp_in_grid_in_rad = efth

    # compute hs
    ef = scipy.integrate.trapz(efth.data, efth.dir)*(180/np.pi)*(180/np.pi)
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_interp_in_grid_in_rad = 4*np.sqrt(m0)

    # step 5 : filter efth based on maximum frequency

    def w(h, k, alpha):
        """ Compute w the angular frequency (in radians per second).
        
        Eq.115 BOSZ manual.

        Parameters
        ----------
        h : float
            The distance from initial water level to the ground surface (also known as 
            bathymetry). It is positive downwards with a maximum at the bottom. Topography 
            is consequently negative.
        k : float
            The wavenumber (in radians per metre).
        alpha : float
            A constant, for Nwogu equations alpha=-0.39.
        """

        # g : earth's gravity, set to 9.81 m=s2
        g = 9.81

        # w
        w = np.sqrt((g*h) * (1-(alpha+1/3)*h*h*k*k) * 1/((1-alpha*h*h*k*k) )) * k
 
        return w

    # good dispersion accuracy up to kmax*h = PI
    kmax = np.pi/h_wavemaker

    # good dispersion accuracy up to wmax:
    wmax = w(h_wavemaker, kmax, -0.39)

    # good dispersion accuracy up to fmax:
    fmax = wmax/(2*np.pi)

    # filter efth based on maximum frequency
    efth = efth.where(efth.freq<=fmax, drop=True)

    # save
    efth_swan_interp_in_grid_in_rad_freqcutoff = efth

    # compute hs
    ef = scipy.integrate.trapz(efth.data, efth.dir)*(180/np.pi)*(180/np.pi)
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_interp_in_grid_in_rad_freqcutoff = 4*np.sqrt(m0)

    # step 6 : compute correction factor

    # integrate efth over directions
    ef = efth.integrate("dir")*(180/np.pi)*(180/np.pi)
    ef.attrs["units"] = "m2/Hz"

    # integrate ef over frequencies
    area = ef.integrate("freq")
    area.attrs["units"] = "m2"

    # integrate efth_unfiltered over directions
    ef_unfiltered = efth_swan_interp_in_grid_in_rad.integrate("dir")*(180/np.pi)*(180/np.pi)
    ef_unfiltered.attrs["units"] = "m2/Hz"

    # integrate ef_unfiltered over frequencies
    area_unfiltered = ef_unfiltered.integrate("freq")
    area_unfiltered.attrs["units"] = "m2"

    # compute correction factor
    cs = area_unfiltered/area
    cs = cs.data

    # step 7 : correct efth based on correction factor

    # correct efth
    efth = efth * cs
    efth.attrs["units"] = "m2/Hz/rad"

    # save
    efth_swan_interp_in_grid_in_rad_freqcutoff_cf = efth

    # compute hs
    ef = scipy.integrate.trapz(efth.data, efth.dir)*(180/np.pi)*(180/np.pi)
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_interp_in_grid_in_rad_freqcutoff_cf = 4*np.sqrt(m0)

    # step 8 : keep only dir between [0, pi/2] and [3pi/2, 2pi]

    efth = efth.where((efth.dir<np.pi/2) | (efth.dir>3*np.pi/2))

    # save
    efth_swan_interp_in_grid_in_rad_freqcutoff_cf_180window = efth

    # compute ef
    # integrate efth over directions : [0, pi/2]
    efth1 = efth.sel(dir=slice(0, np.pi/2))
    ef1 = scipy.integrate.trapz(efth1.data, efth1.dir)*(180/np.pi)*(180/np.pi)
    # integrate efth over directions : [3*pi/2, 2pi]
    efth2 = efth.sel(dir=slice(3*np.pi/2, 2*np.pi))
    ef2 = scipy.integrate.trapz(efth2.data, efth2.dir)*(180/np.pi)*(180/np.pi)
    # add ef1 and ef2
    ef = ef1 + ef2

    # compute hs
    m0 = scipy.integrate.trapz(ef, efth.freq)
    hs_swan_interp_in_grid_in_rad_freqcutoff_cf_180window = 4*np.sqrt(m0)
    hs_swan_interp_in_grid_in_rad_freqcutoff_cf_180window 

    # step 8 : visualization

    # figure 1
    fig1 = go.Figure()

    fig1.add_trace(go.Scatter(
        x=efth.freq.data,
        y=ef
    ))

    # add labels
    fig1.update_xaxes(title="freq [Hz]")
    fig1.update_yaxes(title="ef [m2/Hz]")
    
    # figure 2
    title1 = "Swan spectrum, Hs=" + str(round(hs_swan_in_degrees, 2)) + "m" 
    title2 = "Swan spectrum interpolated, Hs=" + str(round(hs_swan_interp_in_degrees, 2)) + "m"
    title3 = "Swan spectrum interpolated, in the grid's referential Hs=" + str(round(hs_swan_interp_in_grid_in_degrees, 2)) + "m"
    fig2 = plotly.subplots.make_subplots(rows=3, cols=1, shared_xaxes=True,
        subplot_titles=(title1, title2, title3)
        )

    # add trace
    fig2.add_trace(
        go.Heatmap(
            z=efth_swan_in_degrees.data.T,
            x=efth_swan_in_degrees.freq,
            y=efth_swan_in_degrees.dir,
            coloraxis = "coloraxis",
        ),
        row=1,
        col=1,
    )
    # add labels
    fig2.update_xaxes(title="freq [Hz]", row=1, col=1)
    fig2.update_yaxes(title="dir [deg]", row=1, col=1)

    # add trace
    fig2.add_trace(
        go.Heatmap(
            z=efth_swan_interp_in_degrees.data.T,
            x=efth_swan_interp_in_degrees.freq,
            y=efth_swan_interp_in_degrees.dir,
            coloraxis = "coloraxis",
        ),
        row=2,
        col=1,
    )

    # add labels
    fig2.update_xaxes(title="freq [Hz]", row=2, col=1)
    fig2.update_yaxes(title="dir [deg]", row=2, col=1)

    # add trace
    fig2.add_trace(
        go.Heatmap(
            z=efth_swan_interp_in_grid_in_degrees.data.T,
            x=efth_swan_interp_in_grid_in_degrees.freq,
            y=efth_swan_interp_in_grid_in_degrees.dir,
            coloraxis = "coloraxis",
        ),
        row=3,
        col=1,
    )

    # add labels
    fig2.update_xaxes(title="freq [Hz]", row=3, col=1)
    fig2.update_yaxes(title="dir [deg]", row=3, col=1)

    # update title and height
    temp = "Grid forward azimuth=" + \
        str(round(fwd_azimuth, 3)) + "°"
    fig2.update_layout(title_text=temp, height=800)

    fig2.update_layout(
        coloraxis_colorbar=dict(
            title="m2/Hz/deg",
        ),
    )

    # figure 3
    title1 = "Swan spectrum, Hs=" + str(round(hs_swan_interp_in_grid_in_rad, 2)) + "m" 
    title2 = "Swan spectrum frequency cut off, Hs=" + str(round(hs_swan_interp_in_grid_in_rad_freqcutoff, 2)) + "m"
    title3 = "Swan spectrum frequency cut off, correction factor, Hs=" + str(round(hs_swan_interp_in_grid_in_rad_freqcutoff_cf, 2)) + "m"
    title4 = "Swan spectrum frequency cut off, correction factor, only [0, pi/2], [pi/2, 3pi/2] directions, Hs=" + str(round(hs_swan_interp_in_grid_in_rad_freqcutoff_cf_180window, 2)) + "m"

    fig3 = plotly.subplots.make_subplots(rows=4, cols=1, shared_xaxes=True,
        subplot_titles=(title1, title2, title3, title4)
        )

    # add trace
    fig3.add_trace(
        go.Heatmap(
            z=efth_swan_interp_in_grid_in_rad.data.T,
            x=efth_swan_interp_in_grid_in_rad.freq,
            y=efth_swan_interp_in_grid_in_rad.dir,
            coloraxis = "coloraxis",
        ),
        row=1,
        col=1,
    )
    # add labels
    fig3.update_xaxes(title="freq [Hz]", row=1, col=1)
    fig3.update_yaxes(title="dir [rad]", row=1, col=1)

    # add trace
    fig3.add_trace(
        go.Heatmap(
            z=efth_swan_interp_in_grid_in_rad_freqcutoff.data.T,
            x=efth_swan_interp_in_grid_in_rad_freqcutoff.freq,
            y=efth_swan_interp_in_grid_in_rad_freqcutoff.dir,
            coloraxis = "coloraxis",
        ),
        row=2,
        col=1,
    )

    # add labels
    fig3.update_xaxes(title="freq [Hz]", row=2, col=1)
    fig3.update_yaxes(title="dir [rad]", row=2, col=1)

    # add trace
    fig3.add_trace(
        go.Heatmap(
            z=efth_swan_interp_in_grid_in_rad_freqcutoff_cf.data.T,
            x=efth_swan_interp_in_grid_in_rad_freqcutoff_cf.freq,
            y=efth_swan_interp_in_grid_in_rad_freqcutoff_cf.dir,
            coloraxis = "coloraxis",
        ),
        row=3,
        col=1,
    )

    # add labels
    fig3.update_xaxes(title="freq [Hz]", row=3, col=1)
    fig3.update_yaxes(title="dir [rad]", row=3, col=1)

    # add trace
    fig3.add_trace(
        go.Heatmap(
            z=efth_swan_interp_in_grid_in_rad_freqcutoff_cf_180window.data.T,
            x=efth_swan_interp_in_grid_in_rad_freqcutoff_cf_180window.freq,
            y=efth_swan_interp_in_grid_in_rad_freqcutoff_cf_180window.dir,
            coloraxis = "coloraxis",
        ),
        row=4,
        col=1,
    )

    # add labels
    fig3.update_xaxes(title="freq [Hz]", row=4, col=1)
    fig3.update_yaxes(title="dir [rad]", row=4, col=1)

    # update title and height
    temp = "Swan spectra in the grid's referential with direction in radians"
    fig3.update_layout(title_text=temp, height=800)

    fig3.update_layout(
        coloraxis_colorbar=dict(
            title="m2/Hz/rad",
        ),
    )

    # step 9 : compute amplitudes for Barracuda

    if run == "1D":
        # compute amplitude, freq and dir vectors for Barracuda
        freq_out = []
        dir_out = []
        amplitude_out = []
        # loop over freq and dir
        for i in np.arange(len(efth.freq.data)):
                    amplitude = np.sqrt(2*df*ef[i])
                    amplitude_out.append(amplitude)
                    freq_out.append(efth.freq.data[i])
                    dir_out.append(0)

    if run == "2D":
        # compute amplitude, freq and dir vectors for Barracuda
        freq_out = []
        dir_out = []
        amplitude_out = []
        # loop over freq and dir
        for freq in efth.freq.data:
            for dir in efth.dir.data:
                if (dir<np.pi/2) or (dir>3*np.pi/2):
                    amplitude = np.sqrt(2*df*ddir*(180/np.pi)*efth.sel(freq=freq, dir=dir))
                    amplitude_out.append(amplitude.data)
                    freq_out.append(freq)
                    dir_out.append(dir)

    # write spectrum_for_barracuda.txt
    # create dict
    dictt = {"freq": freq_out, "dir": dir_out, "amplitude": amplitude_out}

    # create dataframe
    df = pd.DataFrame(dictt)
    df = df.astype(float)
    df = df.sort_values(by=['dir', 'freq'], ascending= [False, True])
    df = df.fillna(0)
    
    # save
    df.to_csv(p_txt, sep="\t", header=None, index=None)

    return fig1, fig2, fig3